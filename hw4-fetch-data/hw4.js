"use strict";

const urlFilms = "https://ajax.test-danit.com/api/swapi/films";

class Films {
  constructor() {
    this.url = urlFilms;
  }

  getData() {
    return fetch(this.url)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Мережа не відповідає");
        }
        return response.json();
      })
      .then((data) => {
        return data;
      })
      .catch((error) => {
        console.error("Виникла проблема з отриманням даных:", error);
      });
  }

  renderAll() {
    const ul = document.createElement("ul");
    document.body.append(ul);

    return this.getData().then((data) => {
      const arrayRender = data.map(
        ({ openingCrawl, name, episodeId, characters }) => {
          const li = document.createElement("li");
          const episodeIdElement = document.createElement("h2");
          const nameElement = document.createElement("h3");
          const filmDescription = document.createElement("p");

          episodeIdElement.textContent = `Епізод: ${episodeId}`;
          nameElement.textContent = `Назва: ${name}`;
          filmDescription.innerHTML = `Короткий зміст: <br><br> ${openingCrawl}`;

          li.append(episodeIdElement);
          li.append(nameElement);
          li.append(filmDescription);

          const request = characters.map((items) => {
            return fetch(items)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                return data;
              });
          });

          Promise.all(request).then((data) => {
            const charName = data.map(({ name }) => {
              const ulChar = document.createElement("ul");
              const charactersName = document.createElement("li");
              ulChar.append(charactersName);
              charactersName.textContent = name;
              li.append(ulChar);

              return name;
            });
            return charName;
          });
          return li;
        }
      );
      console.log(arrayRender);
      ul.append(...arrayRender);
      return arrayRender;
    });
  }
}

const films = new Films();
films.renderAll();
