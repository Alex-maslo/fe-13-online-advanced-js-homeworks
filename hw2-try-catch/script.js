const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

let divRoot = document.createElement("div");
let ul = document.createElement("ul");
divRoot.setAttribute("root", "id");
document.body.appendChild(divRoot);
divRoot.appendChild(ul);

const findProp = ["author", "name", "price"];
books.forEach((book) => {
  try {
    if (findProp.every((prop) => book.hasOwnProperty(prop))) {
      let li = document.createElement("li");
      li.innerHTML = `${book.name}, ${book.author}, ${book.price}`;
      ul.appendChild(li);
    } else {
      const missingProp = findProp.find((prop) => !book.hasOwnProperty(prop));
      throw new Error(`Немає властивості "${missingProp}"`);
    }
  } catch (e) {
    console.log(e.message);
  }
});
