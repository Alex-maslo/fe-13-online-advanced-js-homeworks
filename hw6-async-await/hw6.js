const buttonGetIp = document.createElement("button");
const ipInfoDiv = document.createElement("div");

document.body.append(buttonGetIp);
document.body.append(ipInfoDiv);

buttonGetIp.textContent = "Знайти по IP";

buttonGetIp.addEventListener("click", async () => {
  try {
    const ipRes = await fetch("https://api.ipify.org/?format=json");
    const ipData = await ipRes.json();
    const ip = ipData.ip;

    const geoRes = await fetch(
      `http://ip-api.com/json/${ip}?fields=status,message,zip,continent,country,region,city,district,query`
    );

    const geoData = await geoRes.json();

    const ipInfo = `
            <p><b>Континент:</b> ${geoData.continent}</p>
            <p><b>Країна:</b> ${geoData.country}</p>
            <p><b>Регіон:</b> ${geoData.region}</p>
            <p><b>Місто:</b> ${geoData.city}</p>
            <p><b>Район:</b> ${geoData.zip}</p>
        `;

    ipInfoDiv.innerHTML = ipInfo;
  } catch (err) {
    console.error(err);
  }
});
