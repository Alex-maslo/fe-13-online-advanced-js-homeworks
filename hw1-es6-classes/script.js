class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get age() {
    return this._age;
  }

  set age(age) {
    this._age = age;
  }

  get salary() {
    return this._salary;
  }

  set salary(salary) {
    this._salary = salary;
  }
}

//

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }

  get lang() {
    return this._lang;
  }

  set lang(lang) {
    this._lang = lang;
  }
}

let user1 = new Programmer("John", 25, 2000, ["JavaScript", "Python"]);
let user2 = new Programmer("Alex", 30, 5000, ["Java", "C++"]);

console.log(user1);
console.log(user1.salary);

console.log(user2);
console.log(user2.salary);
